
require essioc
require iocmetadata
require lobox

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICENAME", "Spk-030RFC:RFS-LO-010")
epicsEnvSet("IPADDR", "spk-lob-003.tn.esss.lu.se")

iocshLoad("$(lobox_DIR)/lobox.iocsh")

pvlistFromInfo("ARCHIVE_THIS", "$(DEVICENAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(DEVICENAME):SavResList")

